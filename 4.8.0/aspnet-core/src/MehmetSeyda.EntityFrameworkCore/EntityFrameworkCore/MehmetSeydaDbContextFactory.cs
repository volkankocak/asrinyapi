﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MehmetSeyda.Configuration;
using MehmetSeyda.Web;

namespace MehmetSeyda.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MehmetSeydaDbContextFactory : IDesignTimeDbContextFactory<MehmetSeydaDbContext>
    {
        public MehmetSeydaDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MehmetSeydaDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MehmetSeydaDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MehmetSeydaConsts.ConnectionStringName));

            return new MehmetSeydaDbContext(builder.Options);
        }
    }
}
