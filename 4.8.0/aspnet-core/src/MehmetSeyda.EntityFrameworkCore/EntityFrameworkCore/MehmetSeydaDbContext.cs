﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MehmetSeyda.Authorization.Roles;
using MehmetSeyda.Authorization.Users;
using MehmetSeyda.MultiTenancy;

namespace MehmetSeyda.EntityFrameworkCore
{
    public class MehmetSeydaDbContext : AbpZeroDbContext<Tenant, Role, User, MehmetSeydaDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public MehmetSeydaDbContext(DbContextOptions<MehmetSeydaDbContext> options)
            : base(options)
        {
        }
    }
}
