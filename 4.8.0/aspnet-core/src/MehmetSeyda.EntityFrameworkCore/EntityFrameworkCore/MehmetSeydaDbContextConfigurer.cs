using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MehmetSeyda.EntityFrameworkCore
{
    public static class MehmetSeydaDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MehmetSeydaDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<MehmetSeydaDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
