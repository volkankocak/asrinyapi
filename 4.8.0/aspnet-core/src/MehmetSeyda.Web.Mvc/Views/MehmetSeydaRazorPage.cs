﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace MehmetSeyda.Web.Views
{
    public abstract class MehmetSeydaRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected MehmetSeydaRazorPage()
        {
            LocalizationSourceName = MehmetSeydaConsts.LocalizationSourceName;
        }
    }
}
