﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace MehmetSeyda.Web.Views
{
    public abstract class MehmetSeydaViewComponent : AbpViewComponent
    {
        protected MehmetSeydaViewComponent()
        {
            LocalizationSourceName = MehmetSeydaConsts.LocalizationSourceName;
        }
    }
}
