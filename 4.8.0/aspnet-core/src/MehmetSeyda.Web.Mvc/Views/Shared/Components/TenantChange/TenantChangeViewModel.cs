using Abp.AutoMapper;
using MehmetSeyda.Sessions.Dto;

namespace MehmetSeyda.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
