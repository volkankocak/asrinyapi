﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MehmetSeyda.Configuration;

namespace MehmetSeyda.Web.Startup
{
    [DependsOn(typeof(MehmetSeydaWebCoreModule))]
    public class MehmetSeydaWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MehmetSeydaWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<MehmetSeydaNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MehmetSeydaWebMvcModule).GetAssembly());
        }
    }
}
