﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using MehmetSeyda.Controllers;

namespace MehmetSeyda.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : MehmetSeydaControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
