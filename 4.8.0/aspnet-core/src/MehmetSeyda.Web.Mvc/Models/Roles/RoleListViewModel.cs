﻿using System.Collections.Generic;
using MehmetSeyda.Roles.Dto;

namespace MehmetSeyda.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
