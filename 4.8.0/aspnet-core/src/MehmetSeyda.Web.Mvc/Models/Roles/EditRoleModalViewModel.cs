﻿using Abp.AutoMapper;
using MehmetSeyda.Roles.Dto;
using MehmetSeyda.Web.Models.Common;

namespace MehmetSeyda.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
