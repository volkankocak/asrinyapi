﻿using System.Collections.Generic;
using MehmetSeyda.Roles.Dto;

namespace MehmetSeyda.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}