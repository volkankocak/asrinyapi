using System.Collections.Generic;
using MehmetSeyda.Roles.Dto;
using MehmetSeyda.Users.Dto;

namespace MehmetSeyda.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
