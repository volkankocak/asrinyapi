using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MehmetSeyda.Configuration;
using MehmetSeyda.EntityFrameworkCore;
using MehmetSeyda.Migrator.DependencyInjection;

namespace MehmetSeyda.Migrator
{
    [DependsOn(typeof(MehmetSeydaEntityFrameworkModule))]
    public class MehmetSeydaMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public MehmetSeydaMigratorModule(MehmetSeydaEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(MehmetSeydaMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                MehmetSeydaConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MehmetSeydaMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
