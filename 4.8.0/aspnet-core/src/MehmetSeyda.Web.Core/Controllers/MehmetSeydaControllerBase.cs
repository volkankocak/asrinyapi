using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace MehmetSeyda.Controllers
{
    public abstract class MehmetSeydaControllerBase: AbpController
    {
        protected MehmetSeydaControllerBase()
        {
            LocalizationSourceName = MehmetSeydaConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
