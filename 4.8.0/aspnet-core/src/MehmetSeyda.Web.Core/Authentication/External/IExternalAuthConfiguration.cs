﻿using System.Collections.Generic;

namespace MehmetSeyda.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
