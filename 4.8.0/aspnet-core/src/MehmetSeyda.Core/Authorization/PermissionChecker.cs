﻿using Abp.Authorization;
using MehmetSeyda.Authorization.Roles;
using MehmetSeyda.Authorization.Users;

namespace MehmetSeyda.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
