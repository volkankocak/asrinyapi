﻿using Abp.MultiTenancy;
using MehmetSeyda.Authorization.Users;

namespace MehmetSeyda.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
