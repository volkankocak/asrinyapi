﻿namespace MehmetSeyda
{
    public class MehmetSeydaConsts
    {
        public const string LocalizationSourceName = "MehmetSeyda";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
