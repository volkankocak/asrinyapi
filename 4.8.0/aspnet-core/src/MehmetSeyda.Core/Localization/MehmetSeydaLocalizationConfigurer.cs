﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace MehmetSeyda.Localization
{
    public static class MehmetSeydaLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(MehmetSeydaConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(MehmetSeydaLocalizationConfigurer).GetAssembly(),
                        "MehmetSeyda.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
