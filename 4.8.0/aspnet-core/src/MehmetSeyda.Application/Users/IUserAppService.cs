using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MehmetSeyda.Roles.Dto;
using MehmetSeyda.Users.Dto;

namespace MehmetSeyda.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
