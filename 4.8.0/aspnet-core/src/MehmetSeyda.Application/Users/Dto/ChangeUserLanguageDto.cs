using System.ComponentModel.DataAnnotations;

namespace MehmetSeyda.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}