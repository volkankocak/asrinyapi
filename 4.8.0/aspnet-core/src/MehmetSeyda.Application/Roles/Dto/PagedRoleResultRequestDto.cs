﻿using Abp.Application.Services.Dto;

namespace MehmetSeyda.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

