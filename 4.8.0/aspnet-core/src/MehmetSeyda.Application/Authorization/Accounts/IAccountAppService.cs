﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MehmetSeyda.Authorization.Accounts.Dto;

namespace MehmetSeyda.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
