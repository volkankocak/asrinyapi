﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MehmetSeyda.MultiTenancy.Dto;

namespace MehmetSeyda.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

