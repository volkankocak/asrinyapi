﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using MehmetSeyda.Configuration.Dto;

namespace MehmetSeyda.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : MehmetSeydaAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
