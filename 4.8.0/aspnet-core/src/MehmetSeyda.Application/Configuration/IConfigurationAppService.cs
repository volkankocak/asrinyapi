﻿using System.Threading.Tasks;
using MehmetSeyda.Configuration.Dto;

namespace MehmetSeyda.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
