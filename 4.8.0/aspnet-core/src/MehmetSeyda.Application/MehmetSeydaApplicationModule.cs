﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MehmetSeyda.Authorization;

namespace MehmetSeyda
{
    [DependsOn(
        typeof(MehmetSeydaCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MehmetSeydaApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MehmetSeydaAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MehmetSeydaApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
