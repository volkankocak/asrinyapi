using Microsoft.AspNetCore.Antiforgery;
using MehmetSeyda.Controllers;

namespace MehmetSeyda.Web.Host.Controllers
{
    public class AntiForgeryController : MehmetSeydaControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
