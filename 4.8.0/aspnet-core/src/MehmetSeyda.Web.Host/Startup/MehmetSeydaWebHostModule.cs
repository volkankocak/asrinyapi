﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MehmetSeyda.Configuration;

namespace MehmetSeyda.Web.Host.Startup
{
    [DependsOn(
       typeof(MehmetSeydaWebCoreModule))]
    public class MehmetSeydaWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MehmetSeydaWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MehmetSeydaWebHostModule).GetAssembly());
        }
    }
}
